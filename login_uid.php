<?php
/**
 * Login UID
 *
 * If a user has logged in with an email, replace it with their uid
 *
 * This plugin requires that a working public_ldap directory be configured.
 *
 * @version @package_version@
 * @author Bob Clough
 * @license GNU GPLv3+
 */
class login_uid extends rcube_plugin
{
    public $task = 'login';

    private $rc;
    private $ldap;

    function init()
    {
        $this->rc = rcmail::get_instance();

        $this->add_hook('authenticate', array($this, 'authenticate'));
    }

    function authenticate($args)
    {
        $this->load_config();

        $field = $this->rc->config->get('login_uid_field');

        if ($this->init_ldap($args['host'])) {
            $results = $this->ldap->search('*', $args['user'], 1);

            if (count($results->records) > 0 ) {
              $args['user'] = $results->records[0][$field];
            }
        }
        else {
            $args['abort'] = true;
            $args['error'] = 'User not found in the addressbook.';
        }
        return $args;
    }

    private function init_ldap($host)
    {
        if ($this->ldap) {
            return $this->ldap->ready;
        }

        $this->load_config();

        $addressbook = $this->rc->config->get('login_uid_addressbook');
        $ldap_config = (array)$this->rc->config->get('ldap_public');
        $match       = $this->rc->config->get('login_uid_match');

        if (empty($addressbook) || empty($match) || empty($ldap_config[$addressbook])) {
            return false;
        }

        $this->ldap = new login_uid_ldap_backend(
            $ldap_config[$addressbook],
            $this->rc->config->get('ldap_debug'),
            $this->rc->config->mail_domain($host),
            $match);

        return $this->ldap->ready;
    }
}

class login_uid_ldap_backend extends rcube_ldap
{
    function __construct($p, $debug, $mail_domain, $search)
    {
        parent::__construct($p, $debug, $mail_domain);
        $this->prop['search_fields'] = (array)$search;
    }
}
